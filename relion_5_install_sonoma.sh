##---------------------------------------------------------------
#	relion 5 installation script 
#	Loosly based on (source)
# 	https://gist.github.com/multimeric/47a4893c97eeb881a34381ceb146c950
#	v 1.0 by Juan.Castillo@biophys.mpg.de
##---------------------------------------------------------------


brew install cmake gcc openmpi fltk fftw libx11 libtiff

export CXX=g++-15
export CC=gcc-15
export OMPI_CXX=g++-15
export OMPI_CC=gcc-15

git clone https://github.com/3dem/relion.git

export CMAKE_C_COMPILER=gcc
export CMAKE_CXX_COMPILER=g++

cd relion
git checkout ver5.0
mkdir -p build
cd build

echo "	"
ech  "	Starting Cmake..."
echo "	"

cmake -DCMAKE_C_COMPILER=/opt/homebrew/bin/gcc-14 -DCMAKE_CXX_COMPILER=/opt/homebrew/bin/g++-14 -DCUDA=OFF -DMPI_C_COMPILER=/opt/homebrew/bin/mpicc -DMPI_CXX_COMPILER=/opt/homebrew/bin/mpicxx -DMPI_INCLUDE_PATH=/opt/homebrew/include/mpi.h -DMPI_LIBRARIES=/opt/homebrew/Cellar/open-mpi/5.0.3_1/lib/libmpi.dylib -DOpenMP_INCLUDE_PATH=/opt/homebrew/include -DOpenMP_LIBRARY=/opt/homebrew/lib/libomp.dylib -DCMAKE_C_FLAGS="-fopenmp" -DCMAKE_CXX_FLAGS="-fopenmp" ..

echo "	"
echo "	Cmake sucessful, Starting make"
echo "	"

export OMPI_CC=/opt/homebrew/bin/gcc-14
export OMPI_CXX=/opt/homebrew/bin/g++-14
make -j 6

echo "	"
echo "	make done, please inspect build/bin"
echo "  Don't forget to add the binaries to your PATH"
echo "	FINISHED"
echo "	"

