#!/bin/bash

#####################################################
#   relion_env_create.sh
#   creates 2 scripts
#   1)  source_me_for_relion.sh
#       to get relion env loead
#   2) submit_relion.sh
#       to submit relion_auto.sh with the relion env
####################################################

# Define the WORKDIR variable
WORKDIR=`echo $PWD`

# Create the source_me_for_relion.sh script
cat <<EOF > source_me_for_relion.sh
export PATH="$WORKDIR/cuda-12.5/bin:$WORKDIR/gcc-5.5.0/bin:$WORKDIR/openmpi-4.1.2/bin:$WORKDIR/relion/build/bin:\$PATH"
export LD_LIBRARY_PATH="$WORKDIR/openmpi-4.1.2/lib:$WORKDIR/cuda-12.5/lib64:$WORKDIR/gcc-5.5.0/build/gcc-5.5.0/lib64:\$LD_LIBRARY_PATH"
EOF

# Make the script executable
chmod +x source_me_for_relion.sh

echo ""
echo "	Environment script source_me_for_relion.sh has been created and made executable."
echo "	Creating submit_relion.sh"

# Route
MY_ENV_PATH=$WORKDIR/source_me_for_relion.sh

# Create submit_relion.sh
cat <<EOL > submit_relion.sh
#!/bin/bash

source $MY_ENV_PATH

$WORKDIR/relion_auto.sh "\$@"
EOL

# Make it executable
chmod +x submit_relion.sh
echo ""
echo "	Script submit_relion.sh created and ready to run"
echo ""
