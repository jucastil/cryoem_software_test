#!/bin/bash

####################################################
#		relion_auto.sh : 
#		run 26 relion tests
#		with very little human interaction
#		v.1.0 by Juan.Castillo@biophys.mpg.de
###################################################

# Function to check if folder exist
check_directory(){	
	if [ -d "$1" ]; then	
		return 0
	else
		return 1
	fi
}


# Check if the parameters are given
EXPECTED_ARGS=2
if [[ $# -ne $EXPECTED_ARGS ]]; then 
	echo "";
	echo "	USAGE: ./relion_auto.sh PROJECT_DIR RUN_MODE";
	echo "	where	" 
	echo "		PROJECT_DIR: full path to \"Movies\""
	echo "		AUTO_MODE:"
	echo "			-full: \"Import\" and 3D class selected from default"
	echo "			-semi: you must do both steps above"
	echo "";
	exit 0
fi

# Reasigning parameters to user-friendly names
RELION_PROJECT_DIR=$1
RUN_MODE=$2

# Check if relion is available, if not, exit with error
if command -v "relion" &> /dev/null; then
	echo "	OK, relion available, continuing"
else
	tput setaf 1;
	echo ""
	echo "	ERROR: relion not found, please load it/compile it and try again"
	echo ""
	tput sgr0;
	exit 1
fi


# Check RELION project directory is existing, if not, exit with error
echo "	project dir:" $RELION_PROJECT_DIR
echo
echo "	Checking if project is existing..." 

check_directory  $RELION_PROJECT_DIR

if [ $? -eq 0 ]; then
	echo "	folder $RELION_PROJECT_DIR exist"
	echo ""
else
	tput setaf 1; echo "	folder $RELION_PROJECT_DIR not found, exiting"; tput sgr0;
	exit 1
fi

# Check run mode
if [[ $RUN_MODE == *"full"* ]]; then 
		tput setaf 4; echo "	Full automation " ; tput sgr0
else
		if [[ $RUN_MODE == *"semi"* ]]; then 
			tput setaf 4; echo "	Semi automation, please do as asked" ; tput sgr0;
		else
			tput setaf 1; echo "	Run mode not found, exiting" ; tput sgr0
			exit 1
		fi
fi	

### START: getting "Import" done
if [[ $RUN_MODE == *"full"* ]]; then 
		## check if 'Import' is already there
		check_directory Import
		if [ $? -eq 0 ]; then
			echo "	folder Import exist, using it"
			echo ""
		else
			tput setaf 1; echo "	Import folder not found, copying it"; tput sgr0;
			cp -Rav ../Extra/Import/ .
		fi
		
else
		if [[ $RUN_MODE == *"semi"* ]]; then 
			tput setaf 2; echo "	Please 'Import' movies on a new shell" ; tput sgr0;
			read -p "	Press 'Enter' when done"
		fi
		## check if 'Import' was created
		check_directory Import
		if [ $? -eq 0 ]; then
			echo "	folder Import created properly"
			echo ""
		else
			tput setaf 1; echo "	Import folder not found, exiting"; tput sgr0;
			exit 1
		fi
fi	




#~ # Run motion correction
tput setaf 2; echo ""; echo "	1 - Motion Corr ######################"; echo "";    tput sgr0;
`which relion_run_motioncorr` --i Import/job001/movies.star --o MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum -1 --use_own  --j 12 --float16 --bin_factor 1 --bfactor 150 --dose_per_frame 1 --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 --gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting  --grouping_for_ps 4  --only_do_unfinished   --pipeline_control MotionCorr/job002/

# Run CTF estimation
tput setaf 2; echo ""; echo "	2 - CTF estimation ######################"; echo "" ; tput sgr0;
`which relion_run_ctffind` --i MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/ -Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 --FStep 500 --dAst 100 --ctffind_exe /mpcdf/soft/local/ctffind_4.1.13/bin/ctffind --ctfWin -1 --is_ctffind4  --fast_search  --use_given_ps  --only_do_unfinished   --pipeline_control CtfFind/job003/

# Run particle picking
tput setaf 2; echo "" ; echo "	3 - Particle picking ######################"; echo "" ; tput sgr0;
`which relion_autopick` --fn_topaz_exe relion_python_topaz --i CtfFind/job003/micrographs_ctf.star --odir AutoPick/job004/ --pickname autopick --LoG  --LoG_diam_min 150 --LoG_diam_max 180 --shrink 0 --lowpass 20 --LoG_adjust_threshold 0 --LoG_upper_threshold 5 --only_do_unfinished   --pipeline_control AutoPick/job004/

# Run particle extraction
tput setaf 2; echo "" ; echo "	4 - Particle extraction ######################" echo "" ; tput sgr0;
`which relion_preprocess_mpi` --i CtfFind/job003/micrographs_ctf.star --coord_list AutoPick/job004/autopick.star --part_star Extract/job005/particles.star --part_dir Extract/job005/ --extract --extract_size 256 --float16  --scale 64 --norm --bg_radius 25 --white_dust -1 --black_dust -1 --invert_contrast  --only_do_unfinished   --pipeline_control Extract/job005/

# Run 2D classification
tput setaf 2; echo "" ; echo "	5 - 2D classification ######################"; echo "" ; tput sgr0;
mkdir -p Class2D/job006
mpirun -n 3 `which relion_refine_mpi` --o Class2D/job006/run --iter 25 --i Extract/job005/particles.star --dont_combine_weights_via_disc --preread_images  --pool 30 --j 2 --pad 2  --ctf  --tau2_fudge 2 --particle_diameter 200 --K 50 --flatten_solvent  --zero_mask  --center_classes  --oversampling 1 --psi_step 12 --offset_range 5 --offset_step 2 --norm --scale  --j 4 --gpu ""  --pipeline_control Class2D/job006/

# Auto-select 2D classes
tput setaf 2; echo "" ;  echo "	6 - Auto-select 2D classes ######################"; echo "" ; tput sgr0;
`which relion_class_ranker` --opt Class2D/job006/run_it025_optimiser.star --o Select/job007/ --fn_sel_parts particles.star --fn_sel_classavgs class_averages.star --fn_root rank --do_granularity_features  --auto_select  --min_score 0.21  --pipeline_control Select/job007/

# Train a Topaz model to pick particles using 2D classified particles
tput setaf 2; echo "" ; echo "	7 - Train a Topaz model ######################"; echo "" ; tput sgr0;
`which relion_autopick` --fn_topaz_exe relion_python_topaz --i CtfFind/job003/micrographs_ctf.star --odir AutoPick/job008/ --pickname autopick --topaz_train --topaz_nr_particles 300 --topaz_train_parts Select/job007/particles.star --gpu "0" --only_do_unfinished   --pipeline_control AutoPick/job008/

# Large scale particle picking using the trained Topaz model
tput setaf 2; echo "" ; echo "	8 - Large scale particle picking using the trained Topaz ######################"; echo "" ;tput sgr0;
mkdir -p AutoPick/job009
`which relion_autopick` --fn_topaz_exe relion_python_topaz --i CtfFind/job003/micrographs_ctf.star --odir AutoPick/job009/ --pickname autopick --topaz_extract --topaz_model AutoPick/job008/model_epoch10.sav --gpu "0" --only_do_unfinished   --pipeline_control AutoPick/job009/

# Run particle extraction
tput setaf 2; echo ""; echo "	9 - Particke extraction ######################"; echo "";  tput sgr0;
`which relion_preprocess_mpi` --i CtfFind/job003/micrographs_ctf.star --coord_list AutoPick/job009/autopick.star --part_star Extract/job010/particles.star --part_dir Extract/job010/ --extract --extract_size 256 --minimum_pick_fom -3 --float16  --scale 64 --norm --bg_radius 25 --white_dust -1 --black_dust -1 --invert_contrast  --only_do_unfinished   --pipeline_control Extract/job010/

# Run 2D classification
tput setaf 2; echo ""; echo "	10 - 2D classification (pass 2) ######################";  echo ""; tput sgr0;
mkdir -p Class2D/job011
`which relion_refine` --o Class2D/job011/run --grad --class_inactivity_threshold 0.1 --grad_write_iter 10 --iter 100 --i Extract/job010/particles.star --dont_combine_weights_via_disc --preread_images  --pool 30 --pad 2  --ctf  --tau2_fudge 2 --particle_diameter 200 --K 100 --flatten_solvent  --zero_mask  --center_classes  --oversampling 1 --psi_step 12 --offset_range 5 --offset_step 2 --norm --scale  --j 12 --gpu ""  --pipeline_control Class2D/job011/

# Auto-select 2D classes
tput setaf 2; echo ""; echo "	11 - Auto-select 2D classes (pass 2)  ######################"; echo ""; tput sgr0;
`which relion_class_ranker` --opt Class2D/job011/run_it100_optimiser.star --o Select/job012/ --fn_sel_parts particles.star --fn_sel_classavgs class_averages.star --fn_root rank --do_granularity_features  --auto_select  --min_score 0.1  --pipeline_control Select/job012/

# 3D Ab Initial reconstruction
tput setaf 2;echo ""; echo "	12 - 3D Ab-Initio  ######################"; echo ""; tput sgr0;
mkdir -p InitialModel/job013
`which relion_refine` --o InitialModel/job013/run --iter 100 --grad --denovo_3dref  --i Select/job012/particles.star --ctf --K 1 --sym C1  --flatten_solvent  --zero_mask  --dont_combine_weights_via_disc --preread_images  --pool 3 --pad 1  --particle_diameter 200 --oversampling 1  --healpix_order 1  --offset_range 6  --offset_step 2 --auto_sampling  --tau2_fudge 4 --j 12 --gpu ""  --pipeline_control InitialModel/job013/ && rm -f InitialModel/job013/RELION_JOB_EXIT_SUCCESS && `which relion_align_symmetry` --i InitialModel/job013/run_it100_model.star --o InitialModel/job013/initial_model.mrc --sym D2 --apply_sym --select_largest_class  --pipeline_control InitialModel/job013/ && touch InitialModel/job013/RELION_JOB_EXIT_SUCCESS

# Run 3D classification
tput setaf 2; echo ""; echo "	13 - 3D Classification  ######################"; echo ""; tput sgr0;
mkdir -p Class3D/job014
mpirun -n 3  `which relion_refine_mpi` --o Class3D/job014/run --i Select/job012/particles.star --ref InitialModel/job013/initial_model.mrc --trust_ref_size --ini_high 50 --dont_combine_weights_via_disc --preread_images  --pool 30 --pad 2  --ctf --iter 25 --tau2_fudge 4 --particle_diameter 200 --K 4 --flatten_solvent --zero_mask --blush  --oversampling 1 --healpix_order 2 --offset_range 5 --offset_step 2 --sym C1 --norm --scale  --j 4 --gpu ""  --pipeline_control Class3D/job014/


#~ ## !!!You need to select a 3D class manually in order to continue!!!##

#~ ############# NO 3D class selected but I copy Select/job002/particles.star directly ######################
if [[ $RUN_MODE == *"full"* ]]; then 
		## check if 'Select' is already there
		check_directory Select
		if [ $? -eq 0 ]; then
			tput setaf 2; echo ""; echo "	folder Select exist, using it"; echo ""; tput sgr0;
			cp -Rav ../Extra/Select/job002/ Select/
			echo "	Done"
		else
			tput setaf 1; echo "	Error, no Select folder, something is wrong, exiting..."; tput sgr0;
			exit 1
		fi
		
else
		if [[ $RUN_MODE == *"semi"* ]]; then 
			tput setaf 2; echo "	Please 'Select' the 3D class on a new shell" ; tput sgr0;
			read -p "	Press 'Enter' when done"
		fi
		## check if 'Import' was created
		check_directory Select
		if [ $? -eq 0 ]; then
			echo "	folder Select present"
			echo ""
		else
			tput setaf 1; echo "	Folder 'Select' not present, something is wrong, exiting..."; tput sgr0;
			exit 1
		fi
fi	


# Re-extract particles to get a higher resolution
tput setaf 2; echo ""; echo "	14 - Re-extract particles ######################"; echo ""; tput sgr0;
mkdir -p Class3D/job014
mpirun -n 3 `which relion_preprocess_mpi` --i CtfFind/job003/micrographs_ctf.star --reextract_data_star Select/job002/particles.star --recenter --recenter_x 0 --recenter_y 0 --recenter_z 0 --part_star Extract/job016/particles.star --pick_star Extract/job016/extractpick.star --part_dir Extract/job016/ --extract --extract_size 360 --minimum_pick_fom -3 --float16  --scale 256 --norm --bg_radius 71 --white_dust -1 --black_dust -1 --invert_contrast  --only_do_unfinished   --pipeline_control Extract/job016/

# Run 3D auto-refine
tput setaf 2; echo ""; echo "	15 - 3D auto-refine  ######################"; echo ""; tput sgr0;
mkdir -p Refine3D/job017
mpirun -n 3 `which relion_refine_mpi` --o Refine3D/job017/run --auto_refine --split_random_halves --i Extract/job016/particles.star --ref Class3D/job014/run_it025_class004.mrc --firstiter_cc --trust_ref_size --ini_high 50 --dont_combine_weights_via_disc --preread_images  --pool 30 --pad 2  --auto_ignore_angles --auto_resol_angles --ctf --particle_diameter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 --norm --scale  --j 4 --gpu ""  --pipeline_control Refine3D/job017/

# Mask creation for further refinement
tput setaf 2; echo ""; echo "	16 - Mask creation ######################"; echo ""; tput sgr0;
mkdir -p MaskCreate/job018
`which relion_mask_create` --i Refine3D/job017/run_class001.mrc --o MaskCreate/job018/mask.mrc --lowpass 15 --ini_threshold 0.01 --extend_inimask 3 --width_soft_edge 8 --j 12  --pipeline_control MaskCreate/job018/

# Post-processing
tput setaf 2; echo ""; echo "	17 - Post processing ######################"; echo ""; tput sgr0;
tput sgr0;
`which relion_postprocess` --mask MaskCreate/job018/mask.mrc --i Refine3D/job017/run_half1_class001_unfil.mrc --o PostProcess/job019/postprocess  --angpix 1.244 --auto_bfac  --autob_lowres 10  --pipeline_control PostProcess/job019/

# Run CTF refinement first:beamtilt second:anisotropic magnification third:per particle CTF fitting
tput setaf 2; echo ""; echo "	18 - CTF refinement : beamtilt  ######################"; echo ""; tput sgr0;
`which relion_ctf_refine` --i Refine3D/job017/run_data.star --f PostProcess/job019/postprocess.star --o CtfRefine/job020/ --fit_beamtilt --kmin_tilt 30 --odd_aberr_max_n 3 --fit_aberr --only_do_unfinished  --j 12  --pipeline_control CtfRefine/job020/

tput setaf 2; echo ""; echo "	19 - CTF refinement :  anisotropic magnification  ######################"; echo ""; tput sgr0;
`which relion_ctf_refine` --i CtfRefine/job020/particles_ctf_refine.star --f PostProcess/job019/postprocess.star --o CtfRefine/job021/ --fit_aniso --kmin_mag 30 --only_do_unfinished  --j 12  --pipeline_control CtfRefine/job021/

tput setaf 2; echo ""; echo "	20 - CTF refinement :  per particle CTF fitting ######################"; echo ""; tput sgr0;
`which relion_ctf_refine` --i CtfRefine/job021/particles_ctf_refine.star --f PostProcess/job019/postprocess.star --o CtfRefine/job022/ --fit_defocus --kmin_defocus 30 --fit_mode fpmff --only_do_unfinished  --j 12  --pipeline_control CtfRefine/job022/

# Run 3D auto-refine after ctfrefine
tput setaf 2; echo ""; echo "	21 - 3D auto-refine after CTF refinement ######################"; echo ""; tput sgr0;
mkdir -p Refine3D/job023
mpirun -n 3  `which relion_refine_mpi` --o Refine3D/job023/run --auto_refine --split_random_halves --i CtfRefine/job022/particles_ctf_refine.star --ref Refine3D/job017/run_class001.mrc --trust_ref_size --ini_high 50 --dont_combine_weights_via_disc --preread_images  --pool 30 --pad 2  --auto_ignore_angles --auto_resol_angles --ctf --particle_diameter 200 --flatten_solvent --zero_mask --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 --norm --scale  --j 4 --gpu ""  --pipeline_control Refine3D/job023/

# Post-processing
tput setaf 2; echo "";  echo "	22 - Post processing ######################"; echo ""; tput sgr0;
`which relion_postprocess` --mask MaskCreate/job018/mask.mrc --i Refine3D/job023/run_half1_class001_unfil.mrc --o PostProcess/job024/postprocess  --angpix 1.244 --auto_bfac  --autob_lowres 10  --pipeline_control PostProcess/job024/

# Run Bayesian polishing
tput setaf 2; echo ""; echo "	23 - Bayesian polishing (1)  ######################"; echo ""; tput sgr0;
mkdir -p Polish/job025
`which relion_motion_refine` --i CtfRefine/job022/particles_ctf_refine.star --f PostProcess/job024/postprocess.star --corr_mic MotionCorr/job002/corrected_micrographs.star --first_frame 1 --last_frame -1 --o Polish/job025/ --float16  --min_p 3000 --eval_frac 0.5 --align_frac 0.5 --params3  --only_do_unfinished  --j 24  --pipeline_control Polish/job025/
tput setaf 2;echo ""; echo "	24 - Bayesian polishing (2)  ######################"; echo ""; tput sgr0;
mkdir -p Polish/job026
`which relion_motion_refine` --i CtfRefine/job022/particles_ctf_refine.star --f PostProcess/job024/postprocess.star --corr_mic MotionCorr/job002/corrected_micrographs.star --first_frame 1 --last_frame -1 --o Polish/job027/ --float16  --params_file Polish/job025/opt_params_all_groups.txt --combine_frames --bfac_minfreq 20 --bfac_maxfreq -1 --only_do_unfinished  --j 30  --pipeline_control Polish/job026/

#~ # Run 3D auto-refine after Bayesian polishing
tput setaf 2;echo ""; echo "	25 - 3D auto-refine after Bayesian polishing  ######################"; echo ""; tput sgr0;
mkdir -p Refine3D/job027
mpirun -n 3  `which relion_refine_mpi` --o Refine3D/job027/run --auto_refine --split_random_halves --i Polish/job027/shiny.star --ref Refine3D/job023/run_half1_class001_unfil.mrc --trust_ref_size --ini_high 8 --dont_combine_weights_via_disc --preread_images  --pool 30 --pad 2  --ctf --particle_diameter 200 --flatten_solvent --zero_mask --solvent_mask MaskCreate/job018/mask.mrc --solvent_correct_fsc  --oversampling 1 --healpix_order 2 --auto_local_healpix_order 4 --offset_range 5 --offset_step 2 --sym D2 --low_resol_join_halves 40 --norm --scale  --j 4 --gpu ""  --pipeline_control Refine3D/job027/

#~ # Local resolution estimation
tput setaf 2;echo ""; echo "	26 - Local resolution estimation "; echo ""; tput sgr0;
`which relion_postprocess_mpi` --locres --i Refine3D/job027/run_half1_class001_unfil.mrc --o LocalRes/job028/relion --angpix 1.244 --adhoc_bfac -30 --mask MaskCreate/job018/mask.mrc  --pipeline_control LocalRes/job028/

tput setaf 2;
echo "------------------------------------------"
echo
echo "	ALL TESTS DONE"
echo
echo "------------------------------------------"
tput sgr0;
