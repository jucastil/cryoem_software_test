# cryoem_software_test

A group of scripts to test the most famous cryo-EM software. 

## TLDR (Too Long Dont Read)

Clone the repo, change the scripts to executable `chmod 777 *` and run
- `relion_install.sh` to install relion on a Linux machine
- `relion_5_install_sonoma.sh` to install relion on a macOS sonoma (brew must be installed) <br>
- `relion_env_create.sh` to create your relion enviroment script <br>
- `relion_auto.sh` to run 26 relion _print command_ tests on linux <br>
- `relion_auto.mac.sh` to run 5 relion _print command_ tests on macOS <br>

## Detailed explanation

- [ ] The script `relion_install.sh` will install in the folder you run it: <br> 
First, **gcc-5.5.0**, then **openmpi-4.1.2**, then **conda**, then **cuda-12.5** at the end **relion**. <br>
You should end with an **independent** relion installation.
- [ ] The script `relion_5_instal_sonoma.sh` will install in your macOS sonoma just **relion** <br>
NOTE: tools like **ctffind**, **openmpi** and similar are not handled by the script at this moment. <br>
- [ ] The script `relion_env_create.sh` will write two scripts to allow you to run `relion_auto.sh`. <br> 
- [ ] The script `relion_auto.sh` will go through 26 relion _print command_ tests using `relion30_tutorial` data. <br> 
Manual commands, like the initial **Import** and **Select 3D class** are circinvented by copying them directly from the `Extra` folder. 
<br> Folders will be made by the script when needed.<br>
- [ ] The script `relion_auto.mac.sh` does the same than `relion_auto.sh` but in a macOS. <br>
Unfortunately, since we don't have a GPU, we can't run all the 26 tests.
- [ ] Run `cryosparc_auto.sh` to do the same for cryosparc.

## Running relion

Let's start by cloning this repo, installing relion, and getting the test data. 

```
git clone https://gitlab.mpcdf.mpg.de/jucastil/cryoem_software_test.git
cd cryoem_software_test
chmod 777 *.sh
./relion_install.sh
./relion_env_create.sh
```
Check that you end up with two scripts, `source_me_for_relion.sh` and `submit_relion.sh` <br>
After this install you should have a working relion.<br>
Test it by running `source source_me_for_relion.sh` and getting the pop-up GUI.<br>
To download the test data, run this: <br>
```
wget ftp://ftp.mrc-lmb.cam.ac.uk/pub/scheres/relion30_tutorial_data.tar
tar -xvf relion30_tutorial_data.tar
```
To run it on a Linux machine, copy `submit_relion.sh` inside `relion30_tutorial_data`. <br>
Run it like this:
```
./submit_relion.sh /path/to/relion30_tutorial/ full
```
To run it on your macOS, copy `relion_auto.mac.sh` inside `relion30_tutorial_data`. <br>
Run it like this:
```
./relion_auto.mac.sh /path/to/relion30_tutorial/ full
```
To run it in our LOCAL cluster, we move `submit_relion.sh` to the `relion30_tutorial` folder and type: <br>
```
sbatch --nodes=1 --ntasks-per-node=6 --constraint=gpu --gres=gpu:2 --partition=p.cryo submit_relion.sh /path/to/relion30_tutorial/Movies/ full
```
Note that we ***can't*** run some of the test if we don't ask for the GPU. <br>
To run it on the CRYO cluster, move `submit_relion.sh` to the `relion30_tutorial` folder and type: <br> 
```
sbatch --nodes=1 --ntasks-per-node=6 --partition=p.cryo submit_relion.sh /path/to/relion30_tutorial/Movies/ full
```

## Support

I'll be happy to include any feature you want. <br>
However, I may fail to add it, since I'm not actively processing. <br>

## Roadmap

- [ ] Deploy the repo (done)
- [ ] Improve the code with error handlers
- [ ] Generate a file with the report, in addition to the run
- [ ] Generate a file with system information (that is, where we run)
- [ ] Implement some kind of upload report mechanism 

## Contributing

Please let me know if you want to contribute.
Any help is kindly appreciated.

## Authors and acknowledgment

Please send an email to 
- Juan Castillo (Juan.Castillo@biophys.mpg.de) Project Owner
- Jan Siu Kei Wong (Jan.Wong@biophys.mpg.de) Project Tester

## License

As usual.

## Project status

As of this moment, first runs sucessful. 
More to come soon, hopefully.