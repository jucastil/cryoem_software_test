#!/bin/bash

# Detect the OS
if [[ "$OSTYPE" == "linux"* ]]; then
	echo ""
    echo "You are using Linux: continuing"
elif [[ "$OSTYPE" == "darwin"* ]]; then
	echo ""
    echo "	You are using macOS: please run the mac installer"
	echo ""
	exit 0
elif [[ "$OSTYPE" == "cygwin" ]]; then
	echo ""
    echo "	You are using Cygwin on Windows: not supported"
	echo ""
	exit 0
elif [[ "$OSTYPE" == "msys" ]]; then
	echo ""
    echo "	You are using Git Bash on Windows: not supported"
	echo ""
	exit 0
elif [[ "$OSTYPE" == "win32" ]]; then
	echo ""
    echo "	You are using Windows: not supported"
	echo ""
	exit 0
else
    echo "	Unknown OS: Exiting..."
	exit 1
fi


tput setaf 3;	#yellow
echo " -----------------------------------------------------------------"
echo "	"
echo "	Welcome to relion HOME installer"
echo "	"
echo " -----------------------------------------------------------------"
tput sgr0;
read -n 1 -s -r -p "	Press any key to continue"

# store the WD for setup
WORKDIR=$PWD
echo "	WORKDIR: " $WORKDIR
start=`date +%s`
tput setaf 3;
echo "	"
echo "	Step 1: getting and compiling gcc"
echo "	"
tput sgr0;
if [ -d "$WORKDIR/gcc-5.5.0" ]; then
	tput setaf 2;	#green
	echo "	"
	echo "	gcc seems to be already installed, skipping"
	echo "	"
	tput sgr0;
else
	wget http://ftp.gnu.org/gnu/gcc/gcc-5.5.0/gcc-5.5.0.tar.gz
	tar -xzf gcc-5.5.0.tar.gz
	cd gcc-5.5.0
	./contrib/download_prerequisites
	mkdir build
	cd build
	../configure --prefix=$PWD/gcc-5.5.0 --disable-multilib
	make -j 10
	make install
fi

export PATH=$WORKDIR/gcc-5.5.0/build/gcc-5.5.0/bin/:$PATH
export LD_LIBRARY_PATH=$WORKDIR/gcc-5.5.0/build/gcc-5.5.0/lib64:$LD_LIBRARY_PATH
which gcc
gcc --version

tput setaf 3;
echo "	"
echo "	Done: gcc 5.5.0 installed"
echo "	"
echo "	Step 2: getting and compiling openpmi with slurm"
echo "	"
tput sgr0;

if [ -d "$WORKDIR/openmpi-4.1.2" ]; then
	tput setaf 2;	#green
	echo "	"
	echo "	openmpi seems to be already installed, skipping"
	echo "	"
	tput sgr0;
else
	# install openmpi with slrum
	wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.2.tar.gz
	tar -xzf openmpi-4.1.2.tar.gz
	cd openmpi-4.1.2
	#we need GCC
	export PATH=$WORKDIR/gcc-5.5.0/build/gcc-5.5.0/bin/:$PATH
	./configure --prefix=$WORKDIR/openmpi-4.1.2 --with-pmi=/usr --with-slurm CC=$WORKDIR/gcc-5.5.0/build/gcc-5.5.0/bin/gcc CXX=$WORKDIR/gcc-5.5.0/build/gcc-5.5.0/bin/g++
	make -j 10
	make install
fi

export PATH=$WORKDIR/openmpi-4.1.2/bin/:$PATH
export LD_LIBRARY_PATH=$WORKDIR/openmpi-4.1.2/lib64:$LD_LIBRARY_PATH
which mpicc
which mpicxx

tput setaf 3;
echo "	"
echo "	Done: openmpi installed"
echo "	"
echo "	Step 3: getting and installing conda"
echo "	"
tput sgr0;

cd $WORKDIR

if [ -d "$WORKDIR/miniconda" ]; then
	tput setaf 2;	#green
	echo "	"
	echo "	miniconda seems to be already installed, skipping"
	echo "	"
	tput sgr0;
else
	# get your own conda/mamba
	wget https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-x86_64.sh
	bash Mambaforge-Linux-x86_64.sh -b -p $PWD/mambaforge
fi

export PATH=$WORKDIR/mambaforge/bin/:$PATH
export LD_LIBRARY_PATH=$WORKDIR/mambaforge/lib64:$LD_LIBRARY_PATH
which conda
conda -V

tput setaf 3;
echo "	"
echo "	Done: conda installed"
echo "	"
echo "	Step 4: installing CUDA 12.5"
echo " "
tput sgr0;

if [ -d "$WORKDIR/cuda-12.5" ]; then
	tput setaf 2;
	echo "	"
	echo "	CUDA seems to be installed, skipping"
	echo " "
	tput sgr0;
else
	# get the cuda installer
	wget https://developer.download.nvidia.com/compute/cuda/12.5.1/local_installers/cuda_12.5.1_555.42.06_linux.run
	chmod 777 cuda_12.5.1_555.42.06_linux.run
	mkdir cuda-12.5
	./cuda_12.5.1_555.42.06_linux.run --silent --toolkit --toolkitpath=$WORKDIR/cuda-12.5
fi

tput setaf 3;
echo "	"
echo "	Done: CUDA 12.5 installed"
echo "	"
echo "	Step 5: getting and installing RELION"
echo "	"
tput sgr0;

cd $WORKDIR

if [ -d "$WORKDIR/relion" ]; then
	tput setaf 2;	#green
	echo "	"
	echo "	relion seems to be already installed, please check"
	echo "	"
	tput sgr0;
else
	git clone https://github.com/3dem/relion.git
	cd relion
	git checkout ver5.0
	git pull
	export PATH="$WORKDIR/cuda-12.5/bin:$PATH"
	export LD_LIBRARY_PATH="$WORKDIR/cuda-12.5/lib64/:$LD_LIBRARY_PATH"
	which nvcc
	conda env create -f environment.yml
	mkdir torch_home
	mkdir build
	cd build/
	#module load mkl/2022.2
	cmake -DCUDA=ON -DCUDA_ARCH=61 -DCudaTexture=ON -DFORCE_OWN_FLTK=ON -DMKLFFT=ON -DMPI_C_COMPILER=mpicc -DMPI_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=gcc \
	-DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$WORKDIR/relion \
	-DPYTHON_EXE_PATH=$WORKDIR/mambaforge/envs/relion-5.0/bin/python \
	-DTORCH_HOME_PATH=$WORKDIR/relion/torch_home \
	-DCMAKE_C_FLAGS=-lm  -DCMAKE_CXX_FLAGS=-lm -Wno-de ..
	# -DCUDA_ARCH=61 is for NVIDIA TITAN Xp NVIDIA TITAN X GeForce GTX 1080 Ti and GeForce GTX 1080 which is sbnode[201-218 and 301], nvcc has difficulties
	# to compile and give (cc: error: unrecognized command line option ‘-std=c++14’) so I added gcc and g++ as compiler
	make -j 10
	make install
	export PATH=$WORKDIR/relion/build/bin:$PATH
fi

tput setaf 3;
echo "	"
echo "	Done: relion installed"
echo "	"
tput sgr0;

tput setaf 3;
end=`date +%s`
runtime=$((end-start))
echo "  "`date +%Y-%m-%d%t%H:%M` ": Runtime (in seconds) : " $runtime
echo " -----------------------------------------------------------------"
echo "	"
echo "	##### Everything done #### "
echo " 	to create the relion environment run:"
echo "	1) chmod 777 relion_env_create.sh"
echo "	2) ./relion_env_create.sh"
echo "	To load the relion environment"
echo "	source source_me_for_relion.sh"
echo "	"
echo " -----------------------------------------------------------------"
tput sgr0;
