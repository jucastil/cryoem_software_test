#!/bin/bash

####################################################
#		relion_auto.mac.sh : 
#		run 8 relion tests on your mac
#		with very little human interaction
#		v.0.1 by Juan.Castillo@biophys.mpg.de
###################################################

start=`date +%s`
tput setaf 4;
echo " ----------------------------------------"
echo "  "`date +%Y-%m-%d%t%H:%M` ": relion tests STARTED "
echo " ----------------------------------------"
tput sgr0;


# Function to check if folder exist
check_directory(){	
	if [ -d "$1" ]; then	
		return 0
	else
		return 1
	fi
}


# Check if the parameters are given
EXPECTED_ARGS=2
if [[ $# -ne $EXPECTED_ARGS ]]; then 
	echo "";
	echo "	USAGE: ./relion_auto.sh PROJECT_DIR RUN_MODE";
	echo "	where	" 
	echo "		PROJECT_DIR: full path to \"Movies\""
	echo "		AUTO_MODE:"
	echo "			-full: \"Import\" and 3D class selected from default"
	echo "			-semi: you must do both steps above"
	echo "";
	exit 0
fi

# Reasigning parameters to user-friendly names
RELION_PROJECT_DIR=$1
RUN_MODE=$2

# Check if relion is available, if not, exit with error
if command -v "relion" &> /dev/null; then
	echo "	OK, relion available, continuing"
else
	tput setaf 1;
	echo ""
	echo "	ERROR: relion not found, please load it/compile it and try again"
	echo ""
	tput sgr0;
	exit 1
fi


# Check RELION project directory is existing, if not, exit with error
echo "	project dir:" $RELION_PROJECT_DIR
echo
echo "	Checking if project is existing..." 

check_directory  $RELION_PROJECT_DIR

if [ $? -eq 0 ]; then
	echo "	folder $RELION_PROJECT_DIR exist"
	echo ""
else
	tput setaf 1; echo "	folder $RELION_PROJECT_DIR not found, exiting"; tput sgr0;
	exit 1
fi

# Check run mode
if [[ $RUN_MODE == *"full"* ]]; then 
		tput setaf 4; echo "	Full automation " ; tput sgr0
else
		if [[ $RUN_MODE == *"semi"* ]]; then 
			tput setaf 4; echo "	Semi automation, please do as asked" ; tput sgr0;
		else
			tput setaf 1; echo "	Run mode not found, exiting" ; tput sgr0
			exit 1
		fi
fi	

### START: getting "Import" done
if [[ $RUN_MODE == *"full"* ]]; then 
		## check if 'Import' is already there
		check_directory Import
		if [ $? -eq 0 ]; then
			echo "	folder Import exist, using it"
			echo ""
		else
			tput setaf 1; echo "	Import folder not found, copying it"; tput sgr0;
			cp -Rav ../Extra/Import .
		fi
		
else
		if [[ $RUN_MODE == *"semi"* ]]; then 
			tput setaf 2; echo "	Please 'Import' movies on a new shell" ; tput sgr0;
			read -p "	Press 'Enter' when done"
		fi
		## check if 'Import' was created
		check_directory Import
		if [ $? -eq 0 ]; then
			echo "	folder Import created properly"
			echo ""
		else
			tput setaf 1; echo "	Import folder not found, exiting"; tput sgr0;
			exit 1
		fi
fi	




#~ # Run motion correction
tput setaf 2; echo ""; echo "	1 - Motion Corr ######################"; echo "";    tput sgr0;
`which relion_run_motioncorr` --i Import/job001/movies.star --o MotionCorr/job002/ --first_frame_sum 1 --last_frame_sum -1 --use_own  --j 12 --float16 --bin_factor 1 --bfactor 150 --dose_per_frame 1 --preexposure 0 --patch_x 5 --patch_y 5 --eer_grouping 32 --gainref Movies/gain.mrc --gain_rot 0 --gain_flip 0 --dose_weighting  --grouping_for_ps 4  --only_do_unfinished   --pipeline_control MotionCorr/job002/

# Run ctffind 
`which relion_run_ctffind` --i MotionCorr/job002/corrected_micrographs.star --o CtfFind/job003/ -Box 512 --ResMin 30 --ResMax 5 --dFMin 5000 --dFMax 50000 --FStep 500 --dAst 100 --ctffind_exe /Users/sbadmin/Downloads/ctffind-4.1.14/ctffind --ctfWin -1 --is_ctffind4  --fast_search  --use_given_ps  --only_do_unfinished   --pipeline_control CtfFind/job003/

# Run particle picking
tput setaf 2; echo "" ; echo "  3 - Particle picking ######################"; echo "" ; tput sgr0;
`which relion_autopick` --fn_topaz_exe relion_python_topaz --i CtfFind/job003/micrographs_ctf.star --odir AutoPick/job004/ --pickname autopick --LoG  --LoG_diam_min 150 --LoG_diam_max 180 --shrink 0 --lowpass 20 --LoG_adjust_threshold 0 --LoG_upper_threshold 5 --only_do_unfinished   --pipeline_control AutoPick/job004/

# Run particle extraction
tput setaf 2; echo "" ; echo "  4 - Particle extraction ######################" echo "" ; tput sgr0;
`which relion_preprocess_mpi` --i CtfFind/job003/micrographs_ctf.star --coord_list AutoPick/job004/autopick.star --part_star Extract/job005/particles.star --part_dir Extract/job005/ --extract --extract_size 256 --float16  --scale 64 --norm --bg_radius 25 --white_dust -1 --black_dust -1 --invert_contrast  --only_do_unfinished   --pipeline_control Extract/job005/

# Run 2D classification
tput setaf 2; echo "" ; echo "  5 - 2D classification ######################"; echo "" ; tput sgr0;
mkdir -p Class2D/job006
mpirun -n 3 `which relion_refine_mpi` --o Class2D/job006/run --iter 25 --i Extract/job005/particles.star --dont_combine_weights_via_disc --preread_images  --pool 30 --j 2 --pad 2  --ctf  --tau2_fudge 2 --particle_diameter 200 --K 50 --flatten_solvent  --zero_mask  --center_classes  --oversampling 1 --psi_step 12 --offset_range 5 --offset_step 2 --norm --scale  --j 4 --gpu ""  --pipeline_control Class2D/job006/

# Auto-select 2D classes
tput setaf 2; echo "" ; echo " 6 - Auto-select 2D classes ###################### : SKIPPED"; echo "" ; tput sgr0;
tput setaf 2; echo "" ; echo " 7 - Train a Topaz model ###################### : SKIPPED"; echo "" ; tput sgr0;
tput setaf 2; echo "" ; echo "  8 - Large scale particle picking using the trained Topaz ######################: SKIPPED"; echo "" ;tput sgr0;

end=`date +%s`
runtime=$((end-start))
tput setaf 4;
echo " -----------------------------------------------------------------"
echo "  "`date +%Y-%m-%d%t%H:%M` ": Runtime (in seconds) : " $runtime
echo " -----------------------------------------------------------------"
echo "  "`date +%Y-%m-%d%t%H:%M` ": TIRF transfer FINISHED "
echo " -----------------------------------------------------------------"
tput sgr0;
